import React, { Component } from "react";
import PropTypes from "prop-types";

export default class Greeting extends Component {
  myAge = 29;

  constructor(props) {
    super(props);
    this.state = {
      age: 28,
    };
  }
  render() {
    return (
      <div>
        <h1>
          Hola {this.props.name} <br></br>
          desde OpenBootcamp en React
        </h1>
        <h2>Tu edad es de: {this.state.age}</h2>
        <div>
          <button onClick={this.birthday}>
            Cumplir annos
          </button>
        </div>
      </div>
    );
  }

  birthday = () => {
    this.setState((prevState) => (
      {
        age: prevState.age + 1
      }
    ));
  };
}

Greeting.propTypes = {
  name: PropTypes.string,
}
