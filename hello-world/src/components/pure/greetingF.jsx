import React, { useState } from "react";
import PropTypes from "prop-types";
import Greeting from './greeting';

const GreetingF = (props) => {
  /**
   * TODOBreve instroduccion a useState
   * @const [variable, metodo para actualizar] = useState(valor inicial)
   */
  const [age, setAge] = useState(29);

  const birthday = () => {
    /**
     * ?actualizamos el State
     */
    setAge(age + 1);
  };

  return (
    <div>
      <h1>Hola! {props.name} desde un componente funcional Greeting!</h1>
      <h2>
        Tu edad es de: { age }
      </h2>
      <div>
      <button onClick={birthday}>
      cumplir annos
      </button>
      </div>
    </div>
  );
};

GreetingF.propTypes = {
  name: PropTypes.string,
};

export default GreetingF;
